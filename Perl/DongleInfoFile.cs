﻿using _CONSTANTS_PPT = Constants_ProductionTool_Perl._Constants_ProductionTool_Perl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace NS_Perl
{
    [XmlRoot]
    public class DongleInfoFile
    {
        [XmlIgnore]
        public static string savePath = $"{_CONSTANTS_PPT.FOLDER_PATH_DONGLE}{_CONSTANTS_PPT.FOLDER_PREFIX_DONGLE}SN##\\";

        [XmlIgnore]
        public static string saveName = $"{_CONSTANTS_PPT.FILE_NAME_DONGLE_INFO}";

        [XmlElement("Dongle")]
        public Dongle xml;

        [XmlIgnore]
        public string PairedPi
        {
            get
            {
                return xml.Perl.SN;
            }
            set
            {
                xml.Perl.SN = value;
                Save();
            }
        }

        [XmlIgnore]
        public string BpCode
        {
            get
            {
                return xml.BP.Code;
            }
            set
            {
                xml.BP.Code = value;
                Save();
            }
        }

        [XmlIgnore]
        public string DvcCode
        {
            get
            {
                return xml.DVC.Code;
            }
            set
            {
                xml.DVC.Code = value;
                Save();
            }
        }

        [XmlIgnore]
        public string LicenseType
        {
            get
            {
                return xml.LicenseType;
            }
            set
            {
                xml.LicenseType = value;
                Save();
            }
        }

        [XmlIgnore]
        public DateTime DatePaired
        {
            get
            {
                return xml.Perl.dateOfPairing;
            }
            set
            {
                xml.Perl.dateOfPairing = value;
                Save();
            }
        }

        public class Dongle
        {


            [XmlAttribute]
            public string SerialNumber = "";

            public string LicenseType = "";

            public string MoType = "";

            public DvcInfo DVC = new DvcInfo();
            public BpInfo BP = new BpInfo();
            public PerlInfo Perl = new PerlInfo();

            public Dongle()
            {
                SerialNumber = "NULL";
            }

            public Dongle(string sn)
            {
                SerialNumber = sn;
            }

            public class DvcInfo
            {
                [XmlAttribute]
                public string Code = "";

                public string LockCode = "";
                public DateTime dateOfRequest = new DateTime();
                public DateTime dateOfReceipt = new DateTime();
            }

            public class BpInfo
            {
                [XmlAttribute]
                public string Code = "";

                public DateTime dateOfRequest = new DateTime();
                public DateTime dateOfReceipt = new DateTime();
            }

            public class PerlInfo
            {
                [XmlAttribute]
                public string SN = "";

                public DateTime dateOfPairing = new DateTime();
            }


        }

        public DongleInfoFile() { xml = new Dongle("NULL"); }

        public DongleInfoFile(string dongleSn)
        {
            xml = new Dongle(dongleSn);
        }

        public void Save()
        {


            XmlSerializer serializer = new XmlSerializer(typeof(DongleInfoFile));

            try
            {
                string fileName = savePath.Replace("SN##", xml.SerialNumber) + saveName.Replace("SN##", xml.SerialNumber);
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                {
                    serializer.Serialize(stream, this);
                    stream.Close();

                }
            }
            catch (DirectoryNotFoundException)
            {

                CreateFolder(xml.SerialNumber);
                Save();
            }


        }

        public static DongleInfoFile Load(string sn)
        {

            DongleInfoFile ret = null;

            string folderPath = savePath.Replace("SN##", sn) + saveName.Replace("SN##", sn);

            XmlSerializer serializer = new XmlSerializer(typeof(DongleInfoFile));
            try
            {

                FileStream stream = new FileStream(folderPath, FileMode.Open);
                using (stream)
                {

                    ret = serializer.Deserialize(stream) as DongleInfoFile;

                    stream.Close();
                }
            }
            catch (System.InvalidOperationException ex)
            {

                if (ex.Message.Contains("There is an error in XML document (1, 1)"))
                {
                    MessageBox.Show($"Need to convert {folderPath} from Ini to XML...");

                    return Load(sn);
                }
                throw;
            }
            catch (FileNotFoundException)
            {
                ret = new DongleInfoFile(sn);
                ret.Save();
                return ret;
            }
            catch (DirectoryNotFoundException)
            {
                CreateFolder(sn);
                return Load(sn);
            }
            catch (IOException)
            {

                throw;
            }


            return ret;
        }

        private static void CreateFolder(string sn)
        {
            Directory.CreateDirectory(savePath.Replace("SN##", sn));
        }
    }
}
