﻿using NS_Perl.NS_LicenseDongle;

namespace NS_Perl
{
    /// <summary>
    /// The base-level Perl Specific Device Info
    /// </summary>
    public struct Perl
    {
        /// <summary>
        /// The customer the Perl System belongs to
        /// </summary>
        public string Customer;

        /// <summary>
        /// The PouchInspector Device information attached to this Perl System
        /// </summary>
        public PouchInspector PouchInspector;

        /// <summary>
        /// The LicenseDongle information attached to this Perl System
        /// </summary>
        public LicenseDongle Dongle;

        /// <summary>
        /// Check to see if the Perl Object was created successfully
        /// </summary>
        public bool IsNotNull => PouchInspector.Sn != "NOTFOUND";

    }

    

    

    
}
