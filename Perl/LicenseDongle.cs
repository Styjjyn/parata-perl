﻿using System;
using System.Collections.Generic;
using System.IO;
using static NS_Perl.PouchInspector;

namespace NS_Perl.NS_LicenseDongle
{
    /// <summary>
    /// The License Dongle information of a Perl System
    /// </summary>
    public struct LicenseDongle
    {
        /// <summary>
        /// The 2nd entry found in a'Machine.lic' file. This value determines which Perl System the dongle can be used with.
        /// </summary>
        public BpPcCode_Ext bpPcCode;

        public string
            Sn,
            BpCode,
            DvcCode,
            LockCode
            ;

        /// <summary>
        /// The licensing process step that the Dongle is currently at
        /// </summary>
        public DongleStatus status;

        /// <summary>
        /// Same as bpPcCode
        /// </summary>
        public UnitType type;

        /// <summary>
        /// Date relevant to the Licensing process
        /// </summary>
        public DateTime

            bpRequestDate,
            bpFulfillmentDate,
            dvcRequestDate,
            dvcFulfillmentDate,
            bpUpgradeRequestDate,
            bpUpgradeFulfillmentDate,
            dateOfPair;

        /// <summary>
        /// A list of the strings used to create the Machine.lic file
        /// </summary>
        private List<string> linesToBuildMachineLic;

        /// <summary>
        /// Creates a 'Machine.lic' file to the specified directory.
        /// </summary>
        /// <param name="fileDestination">The directory where the file will be created</param>
        /// <param name="overwrite">Perform an overwrite if an existing Machine.lic file is found at the fileDestination</param>
        /// <returns></returns>
        public ReturnValues CreateMachineLicFile(string fileDestination, bool overwrite)
        {
            DirectoryInfo dir = new DirectoryInfo(fileDestination);
            

            FileInfo[] machFiles = dir.GetFiles(_CONSTANTS.DONGLE_MACH_LIC_FILENAME);

            if (machFiles != null && machFiles.Length > 0 && overwrite == false)
                return ReturnValues.MACH_LIC_EXISTS;

            foreach (FileInfo f in machFiles)
                f.Delete();

            File.WriteAllLines(fileDestination + "\\" + _CONSTANTS.DONGLE_MACH_LIC_FILENAME, GetMachineLicLines());

            return ReturnValues.SUCCESS;
        }

        /// <summary>
        /// Returns the lines necessary to create a Machine.Lic file
        /// </summary>
        /// <returns></returns>
        public string[] GetMachineLicLines()
        {

            linesToBuildMachineLic = new List<string>
                {
                    "PI_LICENSE_FILE",
                    "00010001",
                    "================================================================"
                };

            foreach (string splitCode in bpPcCode.GetSplitCodes())
            {
                linesToBuildMachineLic.Add(splitCode);
            }

            linesToBuildMachineLic.Add("================================================================");
            linesToBuildMachineLic.Add(BpCode);
            linesToBuildMachineLic.Add("================================================================");

            return linesToBuildMachineLic.ToArray();
        }

        

        

        /// <summary>
        /// The second section of the machine.lic file
        /// DO NOT CHANGE INDECES. THESE MATCH WITH THE DATABASE
        /// </summary>
        public enum BpPcCode
        {
            PI_ONLY = 1, //Shows as 1001 in Machine.lic file
            PI_CR = 2 //Shows as 1001, 1002 in Machine.lic file
        }

        /// <summary>
        /// An extension of the BpPcCode enum. Allows implicit conversions
        /// </summary>
        public class BpPcCode_Ext
        {
            public BpPcCode pcCode;

            public static implicit operator BpPcCode_Ext(int a)
            {
                switch (a)
                {
                    case 0:
                    case 1:
                        return new BpPcCode_Ext { pcCode = BpPcCode.PI_ONLY };
                    default:
                        return new BpPcCode_Ext { pcCode = BpPcCode.PI_CR };
                }
            }

            public string[] GetSplitCodes()
            {
                switch (pcCode)
                {
                    case BpPcCode.PI_ONLY:
                        return new string[] { "1001" };
                    default:
                        return new string[] { "1001", "1002" };
                }
            }
        }

    }

    /// <summary>
    /// Possible return values within the LicenseDongle Object
    /// </summary>
    public enum ReturnValues
    {
        SUCCESS,
        MACH_LIC_EXISTS
    }
}
