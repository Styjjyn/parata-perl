﻿namespace NS_Perl
{
    public static class _CONSTANTS
    {
        /// <summary>
        /// The name that the machine.lic file should be when created onto a PouchInspector
        /// </summary>
        public static string DONGLE_MACH_LIC_FILENAME => "Machine.lic";
    }

    /// <summary>
    /// The setup the dongle will work with.
    /// 
    /// DO NOT CHANGE INDEX. THESE MATCH WITH THE DATABASE
    /// </summary>
    public enum UnitType
    {
        UNSET = 1,
        PI_ONLY = 2,
        PI_CR = 3
    }

    /// <summary>
    /// How the dongle is purchased. This determines the royalty cost to us.
    /// 
    /// DO NOT CHANGE INDEX. THESE MATCH WITH THE DATABASE
    /// </summary>
    public enum MoType
    {
        UNSET = 1,
        CUSTOMER = 2,
        INTERNAL = 3
    }

    /// <summary>
    /// The status of the license processing.
    /// 
    /// DO NOT CHANGE INDEX. THESE MATCH WITH THE DATABASE
    /// </summary>
    public enum DongleStatus
    {
        UNSET = 1,
        REQUESTED_INITIAL = 2,
        ACTIVATED = 3,
        PAIRED = 4,
        REQUESTED_UPGRADE = 5,
        UPGRADED = 6
    }
}
